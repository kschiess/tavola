
require 'clamp'
require 'rack'
require 'unicorn'
require 'pathname'

module Tavola

  class CLI < Clamp::Command
    subcommand 'run', <<-DESC.strip do
        runs tavola on either the current directory or the given directory.
      DESC

      option %w(-d --directory), 'DIRECTORY', 'directory to use as root'

      def execute
        app = lambda do || 
          require 'tavola/configuration'
          require 'tavola/web'
          
          Tavola::Web.set :tavola_config, 
            Tavola::Configuration.new(
              base_dir: Pathname.new(directory || Dir.pwd))
        
          Rack::Builder.new do
            use Rack::ContentLength
            use Rack::Chunked
            use Rack::CommonLogger, $stderr
            use Rack::ShowExceptions
            use Rack::Lint

            run Tavola::Web
          end.to_app
        end

        Unicorn::HttpServer.new(app).start.join
      end
    end
  end

end