
require 'qed/reporter/abstract'

module Tavola
module QED
  class Reporter < ::QED::Reporter::Abstract

    #
    def before_session(session)
      @start_time = Time.now
    end

    def step(step)
      @_explain = step.explain.dup
    end

    #
    def match(step, md)
      unless md[0].empty?
        @_explain.sub!(md[0], md[0])
      end
    end

    #
    def applique(step)
      io.print "#{@_explain}"
      io.print "#{step.example}" #
    end

    #
    def pass(step)
      super(step)
      print_step_heading(step)

      if step.has_example? 
        wrap_example 'green' do
          print_step_example(step)
        end
      end
    end

    #
    def fail(step, error)
      super(step, error)

      tab = step.text.index(/\S/)

      print_step_heading(step)

      if step.has_example? 
        wrap_example('red') do
          print_step_example(step)
        end
      end

      msg = []
      msg << "FAIL: " + error.message.to_s #to_str
      msg << sane_backtrace(error).join("\n")
      msg = msg.join("\n")

      wrap_failure(msg)
    end

    #
    def error(step, error)
      super(step, error)

      raise error if $DEBUG   # TODO: Should this be here?

      tab = step.text.index(/\S/)

      print_step_heading(step)

      if step.has_example? 
        wrap_example('red') do
          print_step_example(step)
        end
      end

      msg = []
      msg << "ERROR: #{error.class} " + error.message #.sub(/for QED::Context.*?$/,'')
      msg << sane_backtrace(error).join("\n")
      msg = msg.join("\n") #

      wrap_failure(msg)
    end

    def after_session(session)
      trap 'INFO', 'DEFAULT' if INFO_SIGNAL
      print_time
      print_tally
    end

  private

    def wrap_failure text
      text = text.
        gsub(%r(^FAIL: (.*)$)) { md = $~ 
          %Q(<span class="text-danger">FAIL</span>: <span class="text-warning">#{md[1]}</span>)}.
        gsub(%r(^ERROR: (\w+) (.*)$)) { md = $~ 
          %Q(<span class="text-danger">ERROR</span>: (<span class="text-primary">#{md[1]}</span>) <span class="text-warning">#{md[2]}</span>) }
      io.puts '<pre>'
      io.puts text
      io.puts '</pre>'
      io.puts
    end
    def wrap_code_block language=nil
      io.puts '~~~' + language.to_s
      yield
      io.puts '~~~'
      io.puts
    end
    def clean_whitespace example
      indent = nil
      lines = example.lines.
        map { |l| 
          md = l.match(/^( +)/)
          indent = md[1].size unless indent

          l[indent..-1] }.join
    end
    def print_step_example step
      if step.data?
        wrap_code_block do
          io.print "#{clean_whitespace(step.example)}"
        end
      else
        wrap_code_block 'ruby' do
          io.print "#{clean_whitespace(step.example)}"
        end
      end
    end
    def print_step_heading step
      io.print "#{@_explain}" unless @_explain.start_with?('~~~')
    end
    def wrap_example color
      io.puts %Q({::options parse_block_html="true" /})
      io.puts %Q(<div class="example #{color}">)
      
      yield

      io.puts %Q(</div>)
      io.puts
    end


  end

end # module QED
end # module Tavola

# Install the reporter below QED::Reporter - it was kind of obvious we'd have
# to do this.
QED::Reporter::Enhance = Tavola::QED::Reporter

