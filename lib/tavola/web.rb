
require 'sinatra/base'

require 'slim'
require 'kramdown'
Tilt.prefer Tilt::KramdownTemplate
require 'tavola/kramdown_rouge'

require 'tavola/demonstrations'

module Tavola
  class Web < Sinatra::Base
    before do
      @all = demonstrations.all
    end

    get '/' do
      slim :'index'
    end

    get '/stylesheets/rouge.css' do
      content_type 'text/css'
      erb :'stylesheets/rouge.css' 
    end

    get '*' do
      return pass unless params[:splat].first.match(/.*\.md$/)

      path = params[:splat].first[1..-1] # relative path
      demonstration = demonstrations.find(path)

      markdown demonstration.render, layout_engine: :slim, layout: :layout
    end

  private
    def demonstrations
      Demonstrations.new(settings.tavola_config)
    end
  end
end