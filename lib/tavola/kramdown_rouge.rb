require 'rouge'

# This little hack here plugs the rouge syntax highlighter into the kramdown
# markdown converter. 
Kramdown::Converter::Html.class_eval do
  def convert_codeblock(el, indent)
    attr = el.attr.dup
    language = extract_code_language!(attr)
    highlight(el.value, language)
  end

  def highlight(code, language=nil, opts={})
    lexer = Rouge::Lexer.find_fancy(language, code) || Rouge::Lexers::PlainText

    options = {
      line_numbers: false, 
      css_class: 'highlight', 
      inline_theme: nil, 
      wrap: true, 
      lexer_options: {}
    }

    highlighter_options = options.to_h.merge(opts)
    highlighter_options[:css_class] = [ highlighter_options[:css_class], lexer.tag ].join(' ')
    lexer_options = highlighter_options.delete(:lexer_options)

    formatter = Rouge::Formatters::HTML.new(highlighter_options)
    formatter.format(lexer.lex(code, lexer_options))
  end
end