
require 'stringio'

require 'qed/settings'
require 'qed/session'
require 'tavola/qed/reporter'

module Tavola
  class Demonstrations
    def initialize config
      @config = config
    end

    def root
      root = @config.base_dir
    end      

    def all
      Dir[root.join('**/*.md')].map { |path|
        Demonstration.new(@config, path) }
    end

    def find path
      Demonstration.new(@config, root.join(path))
    end

  end

  class Demonstration
    attr_reader :path

    def initialize config, path
      @config = config
      @path = path
    end

    def root
      root = @config.base_dir
    end      
    attr_reader :path

    def relative_path
      remove_root(path)
    end

    def in_fork 
      r, w = IO.pipe

      pid = fork do
        yield Channel.new(w)
      end
      pid, status = Process.wait2(pid)

      Channel.new(r).listen
    end

    class Channel
      attr_reader :pipe

      def initialize pipe
        @pipe = pipe
      end

      def tell *msg_parts
        raise "Telling twice is forbidden." unless pipe

        message = Marshal.dump(msg_parts)

        pipe.write [message.size].pack('L')
        pipe.write message

        pipe.close
        @pipe = nil

        msg_parts
      end

      def listen
        raise "Telling twice is forbidden." unless pipe

        size = pipe.read(4).unpack('L').first
        message = pipe.read(size)
        decoded = Marshal.load(message)

        pipe.close
        @pipe = nil

        decoded
      end
    end

    def render
      result = in_fork do |parent|
        begin
          result = qed_augment(path)
          parent.tell(:success, result)
        rescue Exception => ex
          parent.tell(:exception, 
            ex.class, ex.message, ex.backtrace)
        end
      end

      case result.first
        when :success
          return result.last
        when :exception
          klass, message, backtrace = result[1..-1]

          exception = klass.new(message)
          exception.set_backtrace(backtrace)

          raise exception
      else
        raise "NOT REACHED"
      end
    end

    def qed_augment path
      # Mostly a copy from Session#run in QED.
      settings = ::QED::Settings.new()
      session  = ::QED::Session.new(settings)

      io = StringIO.new
      reporter = QED::Reporter.new(io: io)
      
      session.prepare_loadpath
      session.require_libraries

      demo = ::QED::Demo.new(path, at: root)
      demo.steps

      ::QED::Evaluator.run(demo, observers: [reporter], settings: settings)

      io.string
    end

  private
    def remove_root path
      root_len = root.to_s.size

      stripped = path[root_len..-1]
      return stripped[1..-1] if stripped.start_with? '/'
        
      stripped
    end
  end
end