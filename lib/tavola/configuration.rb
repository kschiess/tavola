
module Tavola
  class Configuration
    attr_reader :base_dir

    def initialize opts={}
      @base_dir = opts[:base_dir]
    end
  end
end