# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name     = "tavola"
  s.version  = '0.4.0'
  s.summary  = "Interactive runner for qed tests"
  # s.homepage = "http://todo.project.com/"
  s.authors  = ['Kaspar Schiess']
  s.email    = ["kaspar.schiess@absurd.li"]
    
  s.files         = Dir['**/*']
  s.test_files    = Dir['test/**/*'] + Dir['spec/**/*']
  s.executables   = Dir['bin/*'].map { |f| File.basename(f) }
  s.require_paths = ["lib"]
  
  s.add_runtime_dependency 'ae', '>= 1.8'
  s.add_runtime_dependency 'qed', '>= 2.9'

  s.add_runtime_dependency 'sinatra', '> 1.2', '< 1.5'  
  s.add_runtime_dependency 'padrino-helpers'

  s.add_runtime_dependency 'clamp'  

  s.add_runtime_dependency 'kramdown'  
  s.add_runtime_dependency 'rouge'  
  s.add_runtime_dependency 'slim'  

  s.add_runtime_dependency 'unicorn'  
  s.add_runtime_dependency 'rack'  
end
