
# When all is good

then the example will be highlighted in green. 

    1.assert == 1

# When the assertion fails

the example will be highlighted in red. The assertion failure is printed right after the example. 

    1.assert == 3

# When an exception is raised

the example will be red as well. Exception backtrace is printed below. 

    fail "Some error has occurred."

# Web page access reloads the code

When accessing this web page, your application code is reloaded. The number below changes on every reload.

    include Tavola::Dogfood
    # The number 3 has been determined by rolling a fair dice and is completely random. 
    A_RANDOM_NUMBER         # => 3

# Different styles of formatting code

Code can also be formatted using the `~~~` extension.

~~~ruby
  (1 + 2) # => 3
~~~

# Installation Tips

Use [`guard-livereload`](https://github.com/guard/guard-livereload) and the Chrome LiveReload extension to get immediate test feedback upon saving. Your `Guardfile` would look like this: 

    guard 'livereload', grace_period: 0.1 do
      watch %r(qed/.*)
    end

