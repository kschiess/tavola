require 'guard/guard'

module Guard
  class UnicornHunter < Guard
    # Called on file(s) modifications that the Guard watches.
    # @param [Array<String>] paths the changes files or paths
    # @raise [:task_has_failed] when run_on_change has failed
    def run_on_change(paths)
      if system("killall -m 'tavola worker'")
        puts "Unicorn hunter has found a unicorn..."
      end
    end
  end
end